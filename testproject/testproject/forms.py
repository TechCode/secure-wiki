# -*- coding: utf-8 -*-
import random
import string

from datetime import timedelta
from django.utils import timezone

from django import forms
from wiki.forms import CreateForm

from django.utils.translation import ugettext as _
from django.utils.safestring import mark_safe
from django.forms.util import flatatt
from django.utils.encoding import force_unicode
from django.utils.html import escape, conditional_escape

from itertools import chain

from wiki import models
from wiki.conf import settings
from wiki.editors import getEditor
from wiki.core.diff import simple_merge
from django.forms.widgets import HiddenInput
from wiki.core.plugins.base import PluginSettingsFormMixin
from django.contrib.auth.forms import UserCreationForm
from wiki.core import permissions

from wiki.core.compat import get_user_model
User = get_user_model()

class CreateImageWikiForm(CreateForm):
    
    def __init__(self, request, urlpath_parent, *args, **kwargs):
        super(CreateImageWikiForm, self).__init__(request, urlpath_parent, *args, **kwargs)
        self.request = request
        self.urlpath_parent = urlpath_parent
        self.fields.pop('content')
        self.fields.pop('summary')
        self.fields.keyOrder = ['title', 'slug', 'images']

    images = forms.ImageField()

    def clean(self):
        self.check_spam()
        return self.cleaned_data

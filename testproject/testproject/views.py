# -*- coding: utf-8 -*-
import difflib

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template.context import RequestContext
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext as _
from django.views.generic.base import TemplateView, View
from django.views.generic.edit import FormView
from django.views.generic.list import ListView

from wiki.views.mixins import ArticleMixin
from wiki import editors, models
from wiki.forms import TextInputPrepend
from testproject import forms
from wiki.conf import settings
from wiki.core.plugins import registry as plugin_registry
from wiki.core.diff import simple_merge
from wiki.decorators import get_article, json_view
from django.core.urlresolvers import reverse
from django.db import transaction
from wiki.core.exceptions import NoRootURL
from wiki.core import permissions
from django.http import Http404


class Create(FormView, ArticleMixin):


    form_class = forms.CreateImageWikiForm
    template_name="wiki/uploadpostdirectly.html"

    @method_decorator(get_article(can_write=True, can_create=True))
    def dispatch(self, request, article, *args, **kwargs):
        return super(Create, self).dispatch(request, article, *args, **kwargs)

    def get_form(self, form_class):
        """
        Returns an instance of the form to be used in this view.
        """
        kwargs = self.get_form_kwargs()
        initial = kwargs.get('initial', {})
        initial['slug'] = self.request.GET.get('slug', None)
        kwargs['initial'] = initial
        form = form_class(self.request, self.urlpath, **kwargs)
        form.fields['slug'].widget = TextInputPrepend(prepend='/'+self.urlpath.path)
        return form
    
    @transaction.commit_manually
    def form_valid(self, form):
        user=None
        ip_address = None
        if not self.request.user.is_anonymous():
            user = self.request.user
            if settings.LOG_IPS_USERS:
                ip_address = self.request.META.get('REMOTE_ADDR', None)
        elif settings.LOG_IPS_ANONYMOUS:
            ip_address = self.request.META.get('REMOTE_ADDR', None)
        try:
            
            self.newpath = models.URLPath.create_article(
                self.urlpath,
                form.cleaned_data['slug'],
                title=form.cleaned_data['title'],
                content="",
                user_message="",
                user=user,
                ip_address=ip_address,
                article_kwargs={'owner': user,
                                'group': self.article.group,
                                'group_read': self.article.group_read,
                                'group_write': self.article.group_write,
                                'other_read': self.article.other_read,
                                'other_write': self.article.other_write,
                                })
            messages.success(self.request, _(u"New article '%s' created.") % self.newpath.article.current_revision.title)
        
            transaction.commit()
        # TODO: Handle individual exceptions better and give good feedback.
        except Exception, e:
            transaction.rollback()
            if self.request.user.is_superuser:
                messages.error(self.request, _(u"There was an error creating this article: %s") % str(e))
            else:
                messages.error(self.request, _(u"There was an error creating this article."))
            transaction.commit()
            return redirect('wiki:get', '')
            
        url = self.get_success_url()
        transaction.commit()
        return url
    
    def get_success_url(self):
        return redirect('wiki:get', self.newpath.path)
    
    def get_context_data(self, **kwargs):
        kwargs['parent_urlpath'] = self.urlpath
        kwargs['parent_article'] = self.article
        kwargs['create_form'] = kwargs.pop('form', None)
        kwargs['editor'] = editors.getEditor()
        return super(Create, self).get_context_data(**kwargs)

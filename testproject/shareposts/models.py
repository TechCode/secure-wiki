from django.db import models


class Peer(models.Model):
    identifier = models.CharField(max_length=30, unique=True)
    ip = models.CharField(max_length=30, unique=True, blank=True, null=True)
    last_update = models.DateTimeField()


class WantedArticle(models.Model):
    peer = models.ForeignKey(Peer)
    article = models.SlugField(max_length=50)
    recieved = models.BooleanField()

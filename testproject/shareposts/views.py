from .utils import request_articles, JSONResponse
from .models import WantedArticle, Peer
from wiki.models.article import Article
from wiki.models import URLPath
from rest_framework.parsers import JSONParser
from rest_framework.renderers import JSONRenderer
from django.http import HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from lib import slugify


def select_article(article):
    # if not article.is_valid():
    #     return False

    if URLPath.objects.filter(slug=article) or \
       WantedArticle.objects.filter(article=article):
        return False
    return True


@csrf_exempt
def get_new_article_list(request):
    if request.method == 'POST':
        data = JSONParser().parse(request)
        peer = Peer.objects.get(identifier=data['sender'])
        articles = [
            WantedArticle.objects.create(
                peer=peer,
                article=article,
                recieved=False
            )
            for article in data['articles'] if select_article(article)]
        print articles
        if not article:
            return HttpResponse('No Article Needed', status_code=302)
        request_articles(peer, articles)  # request ASYNC
        # return HttpResponse(" - ".join(data['articles']) + "<br\>" + " - ".join(map(lambda x: x.article, articles)))
        return HttpResponse("<br\> >>>>" + " - ".join(map(lambda x: x.article, articles)))
    return Http404()


class DoesNotExist(Exception):
    pass


def get_article_by_slug(slug):
    try:
        urlpath = URLPath.objects.get(slug=slug)
        return urlpath.article
    except URLPath.DoesNotExist:
        for article in Article.objects.all():
            if slugify(article.current_revision.title) == slug:
                return article
    raise DoesNotExist()

@csrf_exempt
def send_articles(request, slug):
    article = get_article_by_slug(slug)
    data = {
        'slug': article.urlpath_set.all()[0].slug,
        'title': article.current_revision.title,
        'content': article.current_revision.content,
    }
    print data
    return JSONResponse(data)

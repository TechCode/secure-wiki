from django.contrib import admin
from shareposts.models import Peer, WantedArticle


class PeerAdmin(admin.ModelAdmin):
    pass


class WantedArticleAdmin(admin.ModelAdmin):
    pass

admin.site.register(Peer, PeerAdmin)
admin.site.register(WantedArticle, WantedArticleAdmin)

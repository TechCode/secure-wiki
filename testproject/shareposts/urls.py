from shareposts import views
from rest_framework.routers import DefaultRouter


router = DefaultRouter()
router.register(r'snippets', views.AvailableArticleViewSet)
router.register(r'users', views.ArticleViewSet)


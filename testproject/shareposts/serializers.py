from rest_framework import serializers


class AvailableArticleSerializer(serializers.Serializer):
    article = serializers.SlugField()


class ArticleSerializer(serializers.Serializer):
    slug = serializers.SlugField()
    content = serializers.CharField(blank=True)

    title = serializers.CharField(max_length=512)

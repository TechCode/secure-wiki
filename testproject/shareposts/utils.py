from shareposts.models import Peer
from wiki.models import URLPath
from wiki.models.article import Article
from rest_framework.renderers import JSONRenderer
from django.http import HttpResponse
from django.conf import settings

from celery import task
from celery.task.schedules import crontab
from datetime import timedelta
from celery.task import periodic_task

from requests.exceptions import RequestException

from lib import slugify
import requests


def serialize_names(article):
    if article.urlpath_set.reverse()[0].slug:
        return article.urlpath_set.reverse()[0].slug
    return slugify(article.current_revision.title)


@periodic_task(run_every=timedelta(seconds=settings.MAX_SYNC_TIME), name='send-new-articles')
def send_new_articles():
    for peer in Peer.objects.all():
        articles = Article.objects.exclude(
            urlpath__in=URLPath.objects.root_nodes()
        ).filter(
            modified__gte=peer.last_update
        )
        if not articles:
            continue
        new_articles = [
            serialize_names(article)
            for article in articles
        ]
        data = {'sender': settings.IDENTIFIER, 'articles': new_articles}
        new_articles = JSONRenderer().render(data)
        print data
        try:
            conn = requests.post(peer.identifier + '/new_articles/', new_articles)
        except RequestException:
            print 'ConnectionError'
        print conn.status_code
        log = open('/home/moemen/workspace/freedombox/codes/wiki/log.html', 'w')
        log.write(conn.text)


def get_or_create(Model, **kwargs):
    try:
        return Model.objects.get(**kwargs)
    except Model.DoesNotExist:
        return Model.objects.create(**kwargs)


# @task(name='send-new-articles')
def request_articles(peer, wanted_articles):
    for article in wanted_articles:
        # try:
        conn = requests.get(peer.identifier + '/want_articles/{0}'.format(article.article))
        # except RequestException:
            # print 'ConnectionError'
        log = open('/home/moemen/workspace/freedombox/codes/wiki/log_%s.html' % article.article, 'w')
        log.write(conn.text)
        if conn.status_code == 200:
            data = conn.json()
            print data
            URLPath.create_article(
                URLPath.objects.root_nodes()[0],
                data['slug'],
                title=data['title'],
                content=data['content'],
                # user_message=data['summary'],
                article_kwargs={})


class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders it's content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)
